<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <title>{{ $profile->name }}</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('resume-imt/css/aos.css?ver=1.1.0') }}" rel="stylesheet">
    <link href="{{ asset('resume-imt/css/bootstrap.min.css?ver=1.1.0') }}" rel="stylesheet">
    <link href="{{ asset('resume-imt/css/main.css?ver=1.1.0') }}" rel="stylesheet">

    <noscript>
        <style type="text/css">
            [data-aos] {
                opacity: 1 !important;
                transform: translate(0) scale(1) !important;
            }
        </style>
    </noscript>

    <style>
        #typing-data {
            min-height: 22px;
        }

        .progress-container .progress .progress-value {
            top: 5px;
        }

        .progress-container .progress-badge {
            padding-right: 42px;
        }

        .btn-icon {
            display: inline-flex;
            align-items: center;
            justify-content: center;
        }

        .btn-icon svg {
            width: 1.5rem;
            height: 1.5rem;
            fill: #fff;
        }

        .footer-social {
            display: inline-block;
            margin: 5px 1px;
            padding: 0.5rem 0.7rem;
        }

        .footer-social svg {
            width: 2em;
            height: 2em;
            fill: #888888;
            transition: fill .25s ease-in-out;
        }

        .footer-social svg:hover {
            fill: #378c3f;
        }

        .cc-profile-image img {
            object-fit: cover;
        }
    </style>
</head>

<body id="top">
