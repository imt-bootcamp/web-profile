<?php

namespace App\Http\Controllers;

use App\Models\Education;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EducationController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Educations - ' . env('APP_NAME');

        $whereArgs = ['university', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $educations = Education::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.educations.index', compact('title', 'educations'));
    }

    public function create()
    {
        $title = 'Create New Education - ' . env('APP_ENV');

        return view('admin.educations.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_start' => 'required|date',
            'university' => 'required|max:255',
            'level' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        Education::create($request->all());

        return redirect()->route('educations')->with('success', 'Data has been saved.');
    }

    public function edit(Education $education)
    {
        $data = $education;
        $title = "Edit Education {$education->name} - " . env('APP_NAME');

        return view('admin.educations.edit', compact('title', 'data'));
    }

    public function update(Education $education, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_start' => 'required|date',
            'university' => 'required|max:255',
            'level' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $education->update($request->all());

        return redirect()->route('educations')->with('success', 'Data has been updated.');
    }

    public function destroy(Education $education)
    {
        $education->delete();

        return redirect()->route('educations')->with('success', 'Data has been deleted.');
    }
}
