@if ($educations->count())
    <div class="section">
        <div class="container cc-education">
            <div class="h4 text-center mb-4 title">Riwayat Pendidikan</div>

            @foreach ($educations as $education)
                <div class="card">
                    <div class="row">
                        <div class="col-md-3 bg-primary">
                            <div class="card-body cc-education-header">
                                <p>{{ Carbon::parse($education->date_start)->format('M Y') }} - {{ $education->date_end ? Carbon::parse($education->date_end)->format('M Y') : 'Present' }}</p>
                                <div class="h5">{{ $education->level }}</div>
                            </div>
                        </div>
                        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                            <div class="card-body">
                                <div class="h5">{{ $education->title }}</div>
                                <p class="category">{{ $education->university }}</p>
                                <p>{{ $education->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
