@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container px-6 mx-auto grid">
        <div class="flex items-center">
            <a href="{{ route('skills') }}">
                <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-500 h-5 w-5 mr-2 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" /></svg>
            </a>
            <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">Create New Skill</h2>
        </div>

        @include('layouts.alert')

        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
            <form action="{{ route('skills.store') }}" method="POST">
                @csrf

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Name</span>
                    <input
                        name="name"
                        type="text"
                        value="{{ old('name') }}"
                        class="@if ($errors->has('name')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('name')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Value <span id="value"></span></span>
                    <input
                        name="value"
                        type="range"
                        min="0"
                        max="100"
                        value="{{ old('value') ?? 0 }}"
                        class="@if ($errors->has('value')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray"
                    />

                    @error('value')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <div class="flex justify-end">
                    <button id="reset" type="reset" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-gray-600 border border-transparent rounded-lg active:bg-gray-600 hover:bg-gray-700 focus:outline-none focus:shadow-outline-gray mr-4">Reset</button>
                    <button type="submit" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        const inputValueDOM = document.querySelector('[name*="value"]');
        const resetDOM = document.querySelector('#reset');

        function updateTextValue(e) {
            document.getElementById('value').innerText = `(${e.target.value}%)`;
        }

        function updateTextValueMounted(value) {
            document.getElementById('value').innerText = `(${value}%)`;
        }

        function getLastValue(value) {
            document.getElementById('value').innerText = `(${value}%)`;
        }

        if (parseInt(inputValueDOM.value)) {
            updateTextValueMounted(inputValueDOM.value);
        }

        inputValueDOM.addEventListener('input', updateTextValue);
        inputValueDOM.addEventListener('change', updateTextValue);

        resetDOM.addEventListener('click', () => {
            setTimeout(() => {
                getLastValue(inputValueDOM.value);
            }, 0);
        });
    </script>
@endpush
