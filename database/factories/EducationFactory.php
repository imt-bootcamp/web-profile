<?php

namespace Database\Factories;

use App\Models\Education;
use Illuminate\Database\Eloquent\Factories\Factory;

class EducationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Education::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dateStart = $this->faker->date();
        $dateEnd = strtotime('+1 year', strtotime($dateStart));
        $dateEnd = date('Y-m-d', $dateEnd);

        return [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'level' => $this->faker->sentence(3),
            'title' => $this->faker->sentence(3),
            'university' => $this->faker->sentence(3),
            'description' => $this->faker->sentence(),
        ];
    }
}
