<?php

namespace App\Http\View\Composers;

use App\Models\Social;
use Illuminate\View\View;

class SocialComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('socials', Social::orderBy('order')->get());
    }
}
