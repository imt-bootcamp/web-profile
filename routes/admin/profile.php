<?php

use App\Http\Controllers\ProfileController;

Route::group(['prefix' => 'profile'], function () {
    Route::get('', [ProfileController::class, 'edit'])
        ->name('profile.edit');

    Route::patch('{id}', [ProfileController::class, 'update'])
        ->name('profile.update');
});
