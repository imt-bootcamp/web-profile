<?php

use App\Http\Controllers\SkillController;

Route::group(['prefix' => 'skills'], function () {
    Route::get('', [SkillController::class, 'index'])
        ->name('skills');

    Route::get('create', [SkillController::class, 'create'])
        ->name('skills.create');

    Route::post('', [SkillController::class, 'store'])
        ->name('skills.store');

    Route::get('{skill}', [SkillController::class, 'edit'])
        ->name('skills.edit');

    Route::patch('{skill}', [SkillController::class, 'update'])
        ->name('skills.update');

    Route::delete('{skill}', [SkillController::class, 'destroy'])
        ->name('skills.delete');
});
