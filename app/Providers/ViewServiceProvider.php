<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Http\View\Composers\WorkComposer;
use App\Http\View\Composers\SkillComposer;
use App\Http\View\Composers\SocialComposer;
use App\Http\View\Composers\ProfileComposer;
use App\Http\View\Composers\ContactComposer;
use App\Http\View\Composers\EducationComposer;
use App\Http\View\Composers\PortfolioComposer;

class ViewServiceProvider extends ServiceProvider
{
    protected $profileTemplate = [
        'layouts.header',
        'home.*',
    ];

    protected $skillTemplate = [
        'home.skill',
        'home.header',
    ];

    protected $socialTemplate = [
        'home.hero',
        'home.footer',
    ];

    protected $workTemplate = [
        'home.experience',
        'home.header',
    ];

    protected $educationTemplate = [
        'home.education',
    ];

    protected $portfolioTemplate = [
        'home.portfolio',
        'home.header',
    ];

    protected $contactTemplate = [
        'layouts.header',
        'layouts.sidebar',
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer($this->profileTemplate, ProfileComposer::class);
        View::composer($this->skillTemplate, SkillComposer::class);
        View::composer($this->socialTemplate, SocialComposer::class);
        View::composer($this->workTemplate, WorkComposer::class);
        View::composer($this->educationTemplate, EducationComposer::class);
        View::composer($this->portfolioTemplate, PortfolioComposer::class);
        View::composer($this->contactTemplate, ContactComposer::class);
    }
}
