<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Traits\UploadFile;

class Profile extends Model
{
    use HasFactory, UploadFile;

    protected $guarded  = [];

    public function getJobs()
    {
        return explode(', ', $this->jobs);
    }

    public function hasManyJobs()
    {
        $jobs = $this->getJobs();

        if (!is_array($jobs)) {
            return;
        }

        return count($jobs) > 1;
    }
}
