@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container px-6 mx-auto grid">
        <div class="flex items-center">
            <a href="{{ route('educations') }}">
                <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-500 h-5 w-5 mr-2 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" /></svg>
            </a>
            <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">Edit Education {{ $data->name }}</h2>
        </div>

        @include('layouts.alert')

        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
            <form action="{{ route('educations.update', $data) }}" method="POST">
                @method('PATCH')
                @csrf

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Date Start</span>
                    <input
                        name="date_start"
                        type="date"
                        value="{{ old('date_start', $data->date_start) }}"
                        class="@if ($errors->has('date_start')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray"
                    />

                    @error('date_start')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Date End</span>
                    <input
                        name="date_end"
                        type="date"
                        value="{{ old('date_end', $data->date_end) }}"
                        class="@if ($errors->has('date_end')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray"
                    />

                    <span class="block mt-1 text-xs text-gray-600 dark:text-gray-400">
                        * If it's still active, just ignore it.
                    </span>
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">University</span>
                    <input
                        name="university"
                        type="text"
                        value="{{ old('university', $data->university) }}"
                        class="@if ($errors->has('university')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('university')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Level</span>
                    <input
                        name="level"
                        type="text"
                        value="{{ old('level', $data->level) }}"
                        class="@if ($errors->has('level')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray"
                    />

                    @error('level')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Title</span>
                    <input
                        name="title"
                        type="text"
                        value="{{ $data->title ?? old('title') }}"
                        class="@if ($errors->has('title')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray"
                    />

                    @error('title')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Description</span>
                    <textarea
                        rows="6"
                        name="description"
                        class="@if ($errors->has('description')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    >{{ old('description', $data->description) }}</textarea>

                    @error('description')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <div class="flex justify-end">
                    <button id="reset" type="reset" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-gray-600 border border-transparent rounded-lg active:bg-gray-600 hover:bg-gray-700 focus:outline-none focus:shadow-outline-gray mr-4">Reset</button>
                    <button type="submit" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
