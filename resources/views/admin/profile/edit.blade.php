@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container px-6 mx-auto grid">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            <a href="{{ route('profile.edit') }}">Profile</a>
        </h2>

        @include('layouts.alert')

        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
            <form action="{{ route('profile.update', $data->id) }}" enctype="multipart/form-data" method="POST">
                @method('PATCH')
                @csrf

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Background</span>
                    <input
                        type="file"
                        name="background"
                        accept=".png, .jpg, .jpeg"
                        value="{{ $data->background }}"
                        class="@if ($errors->has('background')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('background')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Avatar</span>
                    <input
                        type="file"
                        name="avatar"
                        accept=".png, .jpg, .jpeg"
                        class="block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Name</span>
                    <input
                        name="name"
                        type="text"
                        value="{{ old('name', $data->name) }}"
                        class="@if ($errors->has('name')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('name')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Jobs</span>
                    <input
                        name="jobs"
                        type="text"
                        value="{{ old('jobs', $data->jobs) }}"
                        class="@if ($errors->has('jobs')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('job')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror

                    <span class="block mt-1 text-xs text-gray-600 dark:text-gray-400">
                        * If more then one separate with comma (,)
                    </span>
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">About</span>
                    <textarea
                        rows="6"
                        name="about"
                        class="@if ($errors->has('about')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    >{{ old('about', $data->about) }}</textarea>

                    @error('about')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Birthdate</span>
                    <input
                        type="date"
                        name="birthdate"
                        value="{{ old('birthdate', $data->birthdate) }}"
                        class="@if ($errors->has('birthdate')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('birthdate')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Email</span>
                    <input
                        type="email"
                        name="email"
                        value="{{ old('email', $data->email) }}"
                        class="@if ($errors->has('email')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('email')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">Address</span>
                    <textarea
                        rows="6"
                        name="address"
                        class="@if ($errors->has('address')) border-red-600 @endif block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    >{{ old('address', $data->address) }}</textarea>

                    @error('address')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <label class="block text-sm mb-4">
                    <span class="text-gray-700 dark:text-gray-400">CV</span>
                    <input
                        type="file"
                        name="cv"
                        accept="application/pdf"
                        class="block w-full mt-1 text-sm dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:text-gray-300 dark:focus:shadow-outline-gray form-input"
                    />

                    @error('cv')
                        <span class="block mt-1 text-xs text-red-600 dark:text-red-400">
                            {{ $message }}
                        </span>
                    @enderror
                </label>

                <div class="flex justify-end">
                    <button type="reset" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-gray-600 border border-transparent rounded-lg active:bg-gray-600 hover:bg-gray-700 focus:outline-none focus:shadow-outline-gray mr-4">Reset</button>
                    <button type="submit" class="block px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
