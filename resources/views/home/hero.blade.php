<div class="profile-page">
    <div class="wrapper">
        <div class="page-header page-header-small" filter-color="green">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{ asset($profile->getFile('background')) }})"></div>
            <div class="container">
                <div class="content-center">
                    <div class="cc-profile-image">
                        <a href="{{ $profile->getFile('image', 'no-image-profile.png') }}" target="_blank">
                            <img src="{{ $profile->getFile('image', 'no-image-profile.png') }}" alt="Image"/>
                        </a>
                    </div>

                    <div class="h2 title">{{ $profile->name }}</div>

                    <p class="category text-white" id="typing-data">{{ $profile->hasManyJobs() ? '' : $profile->getJobs()[0] }}</p>

                    <a class="btn btn-primary smooth-scroll mr-2" href="#contact" data-aos="zoom-in" data-aos-anchor="data-aos-anchor">Hire Me</a>

                    @if (File::exists('uploads/' . $profile->cv))
                        <a download class="btn btn-primary" href="{{ $profile->getFile('cv', '', true) }}" data-aos="zoom-in" data-aos-anchor="data-aos-anchor">Download CV</a>
                    @endif
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="button-container">
                        @foreach ($socials as $social)
                            @if ($social->visibility)
                                <a class="btn btn-default btn-round btn-lg btn-icon" href="{{ $social->url }}" rel="tooltip" title="Follow me on {{ $social->name }}">{!! $social->svg !!}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
