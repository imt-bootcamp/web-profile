<?php

namespace App\Http\View\Composers;

use App\Models\Education;
use Illuminate\View\View;

class EducationComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('educations', Education::latest()->get());
    }
}
