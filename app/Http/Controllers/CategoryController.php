<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Categories - ' . env('APP_NAME');

        $whereArgs = ['name', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $categories = Category::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.categories.index', compact('title', 'categories'));
    }

    public function create()
    {
        $title = 'Create New Category - ' . env('APP_ENV');

        return view('admin.categories.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|max:255']);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        Category::create($request->all());

        return redirect()->route('categories')->with('success', 'Data has been saved.');
    }

    public function edit(Category $category)
    {
        $data = $category;
        $title = "Edit Category {$category->name} - " . env('APP_NAME');

        return view('admin.categories.edit', compact('title', 'data'));
    }

    public function update(Category $category, Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|max:255',]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $category->update($request->all());

        return redirect()->route('categories')->with('success', 'Data has been updated.');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('categories')->with('success', 'Data has been deleted.');
    }
}
