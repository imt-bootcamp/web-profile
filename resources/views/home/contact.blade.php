<div class="section" id="contact">
    <div class="cc-contact-information" style="background-image: url({{ asset('resume-imt/images/staticmap.png') }}); background-size: cover; background-repeat: no-repeat;">
        <div class="container">
            <div class="cc-contact">
                <div class="row">
                    <div class="col-md-9">
                        <div class="card mb-0" data-aos="zoom-in">
                            @if (Session::has('success'))
                                <div class="alert-success row contact-message">
                                    <div class="col-md-12">
                                        <p class="font-weight-bold m-0 p-2 small text-center text-uppercase">{{ Session::get('success') }}</p>
                                    </div>
                                </div>
                            @endif

                            @if (Session::has('error'))
                                <div class="alert-danger row contact-message">
                                    <div class="col-md-12">
                                        <p class="font-weight-bold m-0 p-2 small text-center text-uppercase">{{ Session::get('error') }}</p>
                                    </div>
                                </div>
                            @endif

                            <div class="h4 text-center title">Kontak Saya</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card-body">
                                        <form action="{{ route('contact') }}" method="POST">
                                            @csrf

                                            <div class="p pb-3"><strong>Feel free to contact me</strong></div>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-user-circle"></i></span>
                                                        <input class="form-control" type="text" name="name" placeholder="Name" value="{{ old('name') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                                        <input class="form-control" type="text" name="subject" placeholder="Subject" value="{{ old('subject') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input class="form-control" type="email" name="email" placeholder="E-mail" value="{{ old('email') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="message" placeholder="Pesan">{{ old('message') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <button class="btn btn-primary" type="submit">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-body">
                                        <p class="mb-0"><strong>Alamat </strong></p>
                                        <p class="pb-2">{{ $profile->address }}</p>
                                        <p class="mb-0"><strong>Nomor Telepon</strong></p>
                                        <p class="pb-2">{{ $profile->phone }}</p>
                                        <p class="mb-0"><strong>Email</strong></p>
                                        <p>{{ $profile->email }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        const contactMessage = document.querySelector('.contact-message');

        if (contactMessage) {
            setTimeout(() => {
                contactMessage.remove();
            }, 10000);
        }
    </script>
@endpush
