<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function readMessage()
    {
        $this->read = 1;
        $this->save();
    }

    public function unreadMessage()
    {
        $this->read = 0;
        $this->save();
    }

    public static function getUnreadMessage() {
        return Self::where('read',  0)->get();
    }

    public static function hasUnreadMessages()
    {
        return self::getUnreadMessage()->count();
    }

    public function getStatus()
    {
        return $this->read ? 'Read' : 'Unread';
    }
}
