<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SkillController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Skills - ' . env('APP_NAME');

        $whereArgs = ['name', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $skills = Skill::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.skills.index', compact('title', 'skills'));
    }

    public function create()
    {
        $title = 'Create New Skill -' . env('APP_NAME');

        return view('admin.skills.create', compact('title'));
    }

    public function store(Request $request)
    {
        $newRequest = array_merge($request->all(), ['value' => (int) $request->value]);

        $validator = Validator::make($newRequest, [
            'name' => 'required|max:255',
            'value' => 'required|integer|between:0,100',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        Skill::create($request->all());

        return redirect()->route('skills')->with('success', 'Data has been saved.');
    }

    public function edit(Skill $skill)
    {
        $data = $skill;
        $title = "Edit Skill {$skill->name} - " . env('APP_NAME');

        return view('admin.skills.edit', compact('title', 'data'));
    }

    public function update(Skill $skill, Request $request)
    {
        $newRequest = array_merge($request->all(), ['value' => (int) $request->value]);

        $validator = Validator::make($newRequest, [
            'name' => 'required|max:255',
            'value' => 'required|integer|between:0,100',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $skill->update($request->all());

        return redirect()->route('skills')->with('success', 'Data has been updated.');
    }

    public function destroy(Skill $skill)
    {
        $skill->delete();

        return redirect()->route('skills')->with('success', 'Data has been deleted.');
    }
}
