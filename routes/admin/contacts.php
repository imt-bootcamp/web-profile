<?php

use App\Http\Controllers\ContactController;

Route::group(['prefix' => 'contacts'], function () {
    Route::get('', [ContactController::class, 'index'])
        ->name('contacts');

    Route::patch('{contact}/read', [ContactController::class, 'read'])
        ->name('contacts.read');
    Route::patch('{contact}/unread', [ContactController::class, 'unread'])
        ->name('contacts.unread');

    Route::delete('{contact}', [ContactController::class, 'destroy'])
        ->name('contacts.delete');
});
