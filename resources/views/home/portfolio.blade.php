@if ($webDevelopments->count() || $graphicDesigns->count() || $photographies->count())
    <div class="section" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                    <div class="h4 text-center mb-4 title">Portfolio</div>
                    <div class="nav-align-center">
                        <ul class="nav nav-pills nav-pills-primary" role="tablist">
                            @if ($webDevelopments->count())
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#web-development" role="tablist"><i class="fa fa-laptop" aria-hidden="true"></i></a></li>
                            @endif

                            @if ($graphicDesigns->count())
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#graphic-design" role="tablist"><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
                            @endif

                            @if ($photographies->count())
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#photography" role="tablist"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

            <div class="tab-content gallery mt-5">
                @if ($webDevelopments->count())
                    <div class="tab-pane active" id="web-development">
                        <div class="ml-auto mr-auto">
                            @foreach ($webDevelopments as $webDevelopment)
                                <div class="col-md-6">
                                    <div class="cc-porfolio-image img-raised">
                                        <a href="{{ $webDevelopment->url ?? '#web-development' }}">
                                            <figure class="cc-effect">
                                                <img src="{{ $webDevelopment->getFile('photo', 'no-image-portfolio.png') }}" alt="{{ $webDevelopment->title }} photo from ..."/>
                                                <figcaption>
                                                    <div class="h4">{{ $webDevelopment->title }}</div>
                                                    <p>{{ $webDevelopment->category->name }}</p>
                                                </figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                @if ($graphicDesigns->count())
                    <div class="tab-pane" id="graphic-design" role="tabpanel">
                        <div class="ml-auto mr-auto">
                            @foreach ($graphicDesigns as $graphicDesign)
                                <div class="col-md-6">
                                    <div class="cc-porfolio-image img-raised">
                                        <a href="{{ $graphicDesign->url ?? '#graphic-design' }}">
                                            <figure class="cc-effect">
                                                <img src="{{ $graphicDesign->getFile('photo', 'no-image-portfolio.png') }}" alt="{{ $graphicDesign->title }} photo from ..."/>
                                                <figcaption>
                                                    <div class="h4">{{ $graphicDesign->title }}</div>
                                                    <p>{{ $graphicDesign->category->name }}</p>
                                                </figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                @if ($photographies->count())
                    <div class="tab-pane" id="photography" role="tabpanel">
                        <div class="ml-auto mr-auto">
                            @foreach ($photographies as $photography)
                                <div class="col-md-6">
                                    <div class="cc-porfolio-image img-raised">
                                        <a href="{{ $photography->url ?? '#photography' }}">
                                            <figure class="cc-effect">
                                                <img src="{{ $photography->getFile('photo', 'no-image-portfolio.png') }}" alt="{{ $photography->title }} photo from ..."/>
                                                <figcaption>
                                                    <div class="h4">{{ $photography->title }}</div>
                                                    <p>{{ $photography->category->name }}</p>
                                                </figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @php
        $counts = [$webDevelopments->count(), $graphicDesigns->count(), $photographies->count()];
    @endphp

    @push('scripts')
        <script>
            const tabPanes = document.querySelectorAll('.tab-content > .tab-pane');
            const grids = ['#web-development > div', '#graphic-design > div', '#photography > div'];

            let counts = JSON.stringify(@json($counts));
                counts = JSON.parse(counts);

            tabPanes.forEach((tabPane) => tabPane.setAttribute('style', 'display: block;'));

            counts.forEach((count, idx) => {
                if (count) {
                    new Isotope(grids[idx]);
                }
            });

            tabPanes.forEach((tabPane) => tabPane.setAttribute('style', ''));
        </script>
    @endpush
@endif
