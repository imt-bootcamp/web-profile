<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait UploadFile {
    public function getFile($column, $defaultFileName = 'no-image.png', $file = false)
    {
        $fileNotExist = !File::exists('uploads/' . $this[$column]);

        if ($this[$column] == null || $fileNotExist) {
            return !$file ? "/img/{$defaultFileName}" : '';
        }

        return '/uploads/' . $this[$column];
    }

    public function uploadFile($column, $file, $currFile = null)
    {
        if ($file == null) {
            return;
        }

        $this->removeFile($currFile);

        $filename = Str::random(10) . '.' . $file->extension();

        $file->storeAs('uploads', $filename);

        $this[$column] = $filename;
        $this->save();
    }

    public function removeFile($currFile)
    {
        $notMatch = $currFile != null || str_contains($currFile, 'no-image');

        if ($notMatch) {
            Storage::delete('uploads/' . $currFile);
        }
    }
}
