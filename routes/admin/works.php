<?php

use App\Http\Controllers\WorkController;

Route::group(['prefix' => 'works'], function () {
    Route::get('', [WorkController::class, 'index'])
        ->name('works');

    Route::get('create', [WorkController::class, 'create'])
        ->name('works.create');

    Route::post('', [WorkController::class, 'store'])
        ->name('works.store');

    Route::get('{work}', [WorkController::class, 'edit'])
        ->name('works.edit');

    Route::patch('{work}', [WorkController::class, 'update'])
        ->name('works.update');

    Route::delete('{work}', [WorkController::class, 'destroy'])
        ->name('works.delete');
});
