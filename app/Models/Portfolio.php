<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Traits\UploadFile;

class Portfolio extends Model
{
    use HasFactory, UploadFile;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
