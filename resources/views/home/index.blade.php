@include('home.head')
@include('home.header')

<div class="page-content">
    <div>
        @include('home.hero')
        @include('home.about')
        @include('home.skill')
        @include('home.portfolio')
        @include('home.experience')
        @include('home.education')
        @include('home.contact')
    </div>
</div>

@include('home.footer')
