<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PortfolioController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Portfolios - ' . env('APP_NAME');

        $whereArgs = ['title', 'LIKE', "%{$request->q}%"];
        $orderByArgs = ['title', $request->order ?? 'desc'];

        $portfolios = [];

        if ($request['order-by'] == 'category') {
            $joinArgs = ['categories', 'portfolios.category_id', '=', 'categories.id'];
            $orderByArgs = ['categories.name', $request->order ?? 'desc'];

            $portfolios = Portfolio::join(...$joinArgs)
                ->select('portfolios.*')
                ->orderBy(...$orderByArgs)
                ->where(...$whereArgs)
                ->get();
        } else {
            $portfolios = Portfolio::where(...$whereArgs)
                ->orderBy(...$orderByArgs)
                ->get();
        }

        return view('admin.portfolios.index', compact('title', 'portfolios'));
    }

    public function create()
    {
        $title = 'Create New Portfolio - ' . env('APP_ENV');
        $categories = Category::all();

        return view('admin.portfolios.create', compact(
            'title',
            'categories',
        ));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'photo' => 'required|mimes:jpg,png',
            'category_id' => 'required|integer',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $portfolio = Portfolio::create($request->all());
        $portfolio->uploadFile('photo', $request->file('photo'));

        return redirect()->route('portfolios')->with('success', 'Data has been saved.');
    }

    public function edit(Portfolio $portfolio)
    {
        $data = $portfolio;
        $title = "Edit Portfolio {$portfolio->name} - " . env('APP_NAME');
        $categories = Category::all();

        return view('admin.portfolios.edit', compact('title', 'data', 'categories'));
    }

    public function update(Portfolio $portfolio, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'photo' => 'mimes:jpg,png',
            'category_id' => 'required|integer',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $portfolio->update($request->all());
        $portfolio->uploadFile('photo', $request->file('photo'), $portfolio->photo);

        return redirect()->route('portfolios')->with('success', 'Data has been updated.');
    }

    public function destroy(Portfolio $portfolio)
    {
        $portfolio->removeFile($portfolio->photo);
        $portfolio->delete();

        return redirect()->route('portfolios')->with('success', 'Data has been deleted.');
    }
}
