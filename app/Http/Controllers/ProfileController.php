<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function edit()
    {
        $data = Profile::firstOrFail();
        $title = 'Profile - ' . env('APP_NAME');

        return view('admin.profile.edit', compact('data', 'title'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'mimes:jpg,png',
            'name' => 'required|max:255',
            'jobs' => 'required',
            'about' => 'required',
            'birthdate' => 'required|date',
            'email' => 'required|email|max:255',
            'address' => 'required',
            'cv' => 'mimes:pdf',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $profile = Profile::findOrFail($id);
        $currAvatar = $profile->avatar;
        $currBackground = $profile->background;
        $currCV = $profile->cv;

        $profile->update($request->all());
        $profile->uploadFile('background', $request->file('background'), $currBackground);
        $profile->uploadFile('avatar', $request->file('avatar'), $currAvatar);
        $profile->uploadFile('cv', $request->file('cv'), $currCV);

        return back()->with('success', 'Data has been updated.');
    }
}
