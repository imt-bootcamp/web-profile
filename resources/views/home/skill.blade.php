@if ($skillRows->count())
    <div class="section" id="skill">
        <div class="container">
            <div class="h4 text-center mb-4 title">Skill</div>

            <div class="card" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                <div class="card-body">
                    @foreach ($skillRows as $skills)
                        <div class="row">
                            @foreach ($skills as $skill)
                                <div class="col-md-6">
                                    <div class="progress-container progress-primary">
                                        <span class="progress-badge">{{ $skill->name }}</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="{{ $skill->value }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $skill->value }}%;"></div>
                                            <span class="progress-value">{{ $skill->value }}%</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
