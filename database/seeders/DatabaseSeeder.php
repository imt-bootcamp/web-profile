<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Work;
use App\Models\Skill;
use App\Models\Social;
use App\Models\Profile;
use App\Models\Category;
use App\Models\Education;
use App\Models\Portfolio;

class DatabaseSeeder extends Seeder
{
    public $categories = [
        'Web Development',
        'Graphic Design',
        'Photography',
    ];

    public function run()
    {
        User::factory()->create();
        Profile::factory()->create();
        Skill::factory(10)->create();

        foreach ($this->categories as $category) {
            Category::factory()->create(['name' => $category]);
        }

        Portfolio::factory(10)->create();
        Social::factory()->create();
        Work::factory(3)->create();
        Education::factory()->create();
    }
}
