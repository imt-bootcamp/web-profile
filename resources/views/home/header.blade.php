<header>
    <div class="profile-page sidebar-collapse">
        <nav class="navbar navbar-expand-lg fixed-top navbar-transparent bg-primary" color-on-scroll="400">
            <div class="container">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="#" rel="tooltip">{{ $profile->name }}</a>
                </div>

                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-bar bar1"></span><span class="navbar-toggler-bar bar2"></span><span class="navbar-toggler-bar bar3"></span></button>

                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item"><a class="nav-link smooth-scroll" href="#about">Tentang Saya</a></li>

                        @if ($skillRows->count())
                            <li class="nav-item"><a class="nav-link smooth-scroll" href="#skill">Skill</a></li>
                        @endif

                        @if ($webDevelopments->count() || $graphicDesigns->count() || $photographies->count())
                            <li class="nav-item"><a class="nav-link smooth-scroll" href="#portfolio">Portfolio</a></li>
                        @endif

                        @if ($works->count())
                            <li class="nav-item"><a class="nav-link smooth-scroll" href="#experience">Pengalaman</a></li>
                        @endif

                        <li class="nav-item"><a class="nav-link smooth-scroll" href="#contact">Kontak</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
