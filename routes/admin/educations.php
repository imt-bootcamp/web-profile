<?php

use App\Http\Controllers\EducationController;

Route::group(['prefix' => 'educations'], function () {
    Route::get('', [EducationController::class, 'index'])
        ->name('educations');

    Route::get('create', [EducationController::class, 'create'])
        ->name('educations.create');

    Route::post('', [EducationController::class, 'store'])
        ->name('educations.store');

    Route::get('{education}', [EducationController::class, 'edit'])
        ->name('educations.edit');

    Route::patch('{education}', [EducationController::class, 'update'])
        ->name('educations.update');

    Route::delete('{education}', [EducationController::class, 'destroy'])
        ->name('educations.delete');
});
