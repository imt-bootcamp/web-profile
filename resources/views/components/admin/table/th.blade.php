@php
    $label = $attributes->get('label');
    $route = $attributes->get('route');
    $column = $attributes->get('column');

    $order = Request::get('order');
    $currentQueryString = Request::all();

    $queryString = [
        'order-by' => $column,
        'order' => $order ? $order === 'desc' ? 'asc' : 'desc' : 'desc',
    ];

    $queryString = http_build_query(array_merge($currentQueryString, $queryString));
@endphp

<th {{ $attributes->merge(['class' => 'px-4 py-3']) }}>
    <a class="flex items-center gap-2" href="{{ route($route) . '?' . $queryString }}">
        <span>{{ $label }}</span>

        @if (Request::get('order-by') === $column)
            @if ($order === 'desc')
                <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 15l7-7 7 7" /></svg>
            @else
                <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" /></svg>
            @endif
        @endif
    </a>
</th>
