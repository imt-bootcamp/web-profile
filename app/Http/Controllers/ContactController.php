<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Messages - ' . env('APP_NAME');

        $whereArgs = ['name', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $contacts = Contact::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.contacts.index', compact('contacts', 'title'));
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();

        return redirect()->route('contacts')->with('success', 'Data has been deleted.');
    }

    public function read(Contact $contact)
    {
        $contact->readMessage();

        return back();
    }

    public function unread(Contact $contact)
    {
        $contact->unreadMessage();

        return back();
    }
}
