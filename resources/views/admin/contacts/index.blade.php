@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container px-6 mx-auto grid">
        <div class="flex items-center">
            <a href="{{ route('dashboard') }}">
                <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-500 h-5 w-5 mr-2 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" /></svg>
            </a>
            <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
                <a href="{{ route('contacts') }}">Messages</a>
            </h2>
        </div>

        @include('layouts.alert')

        <div class="flex items-center justify-between mb-8 text-sm font-semibold text-white">
            <form action="{{ route('contacts') }}" method="GET">
                <div class="relative text-gray-500 focus-within:text-purple-600 w-80">
                    <input class="block w-full pr-20 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" placeholder="Ledi Hildawan" name="q" value="{{ Request::get('q') }}" />
                    <button type="submit" class="absolute inset-y-0 right-0 px-2 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-r-md active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" /></svg>
                    </button>
                </div>
            </form>
        </div>

        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
            <table class="w-full whitespace-no-wrap">
                <thead>
                    <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
                        <x-admin.table.th class="w-96" column="name" label="Name" route="contacts" />
                        <x-admin.table.th class="w-36" column="read" label="Status" route="contacts" />
                        <x-admin.table.th class="w-36" column="created_at" label="Created At" route="contacts" />
                        <th class="px-4 py-3 w-8"></th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
                    @foreach ($contacts as $contact)
                        <tr class="text-gray-700 dark:text-gray-400">
                            <td class="px-4 py-3 text-sm {{ $contact->read ? '' : 'font-semibold' }}">
                                <div class="text-base font-semibold">{{ $contact->name }}</div>
                                <div class="mt-1 italic underline">{{ $contact->subject }}</div>
                                <div class="mt-2 whitespace-normal">{{ $contact->message }}</div>
                            </td>
                            <td class="px-4 py-3 text-sm">
                                @php
                                    $statusColor = $contact->getStatus() === 'Read' ? 'purple' : 'red';
                                @endphp

                                <span class="px-2 py-1 font-semibold leading-tight text-{{ $statusColor }}-700 bg-{{ $statusColor }}-100 rounded-full dark:text-white dark:bg-{{ $statusColor }}-600">
                                    {{ $contact->getStatus() }}
                                </span>
                            </td>
                            <td class="px-4 py-3 text-sm">{{ Carbon::parse($contact->created_at)->diffForHumans() }}</td>
                            <td class="px-4 py-3">
                                <div class="relative" x-data="{ open: false }">
                                    <svg @click="open = true" @keydown.escape="open = false" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z" /></svg>

                                    <template x-if="open">
                                        <ul x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" @click.away="open = false" @keydown.escape="open = false" class="absolute right-0 w-42 p-2 mt-2 space-y-2 text-gray-600 bg-white border border-gray-100 rounded-md shadow-md dark:border-gray-700 dark:text-gray-300 dark:bg-gray-700 z-10" aria-label="submenu">
                                            <li class="flex">
                                                <a class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-gray-100 hover:text-gray-800 dark:hover:bg-gray-800 dark:hover:text-gray-200" href="mailto:{{ $contact->email }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6" /></svg>

                                                    <span>Reply</span>
                                                </a>
                                            </li>

                                            @if ($contact->getStatus() === 'Read')
                                                <form action="{{ route('contacts.unread', $contact)}}" method="POST">
                                                    @method('PATCH')
                                                    @csrf

                                                    <li class="flex">
                                                        <a class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-gray-100 hover:text-gray-800 dark:hover:bg-gray-800 dark:hover:text-gray-200" href="#" onclick="return confirm('Are you sure you want to delete this data?') ? !event.preventDefault() && this.closest('form').submit() : false;">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" /></svg>
                                                            <span>Mark as unread</span>
                                                        </a>
                                                    </li>
                                                </form>
                                            @endif

                                            @if ($contact->getStatus() === 'Unread')
                                                <form action="{{ route('contacts.read', $contact)}}" method="POST">
                                                    @method('PATCH')
                                                    @csrf

                                                    <li class="flex">
                                                        <a class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-red-100 hover:text-red-800 dark:hover:bg-gray-800 dark:hover:text-gray-200" href="#" onclick="return confirm('Are you sure you want to delete this data?') ? !event.preventDefault() && this.closest('form').submit() : false;">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 19v-8.93a2 2 0 01.89-1.664l7-4.666a2 2 0 012.22 0l7 4.666A2 2 0 0121 10.07V19M3 19a2 2 0 002 2h14a2 2 0 002-2M3 19l6.75-4.5M21 19l-6.75-4.5M3 10l6.75 4.5M21 10l-6.75 4.5m0 0l-1.14.76a2 2 0 01-2.22 0l-1.14-.76" /></svg>
                                                            <span>Mark as read</span>
                                                        </a>
                                                    </li>
                                                </form>
                                            @endif

                                            <form action="{{ route('contacts.delete', $contact) }}" method="POST">
                                                @method('DELETE')
                                                @csrf

                                                <li class="flex">
                                                    <a class="inline-flex items-center w-full px-2 py-1 text-sm font-semibold transition-colors duration-150 rounded-md hover:bg-red-100 hover:text-red-800 dark:hover:bg-gray-800 dark:hover:text-gray-200" href="#" onclick="return confirm('Are you sure you want to delete this data?') ? !event.preventDefault() && this.closest('form').submit() : false;">
                                                        <svg class="w-4 h-4 mr-3" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" /></svg>
                                                        <span>Delete</span>
                                                    </a>
                                                </li>
                                            </form>
                                        </ul>
                                    </template>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            @if(!$contacts->count())
                @include('empty-state')
            @endif
        </div>

        @if ($contacts->count())
            <div class="text-sm">{{ $contacts->count() . ' ' . Str::plural('result', $contacts->count()) }}</div>
        @endif
    </div>
@endsection
