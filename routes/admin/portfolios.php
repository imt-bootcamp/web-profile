<?php

use App\Http\Controllers\PortfolioController;

Route::group(['prefix' => 'portfolios'], function () {
    Route::get('', [PortfolioController::class, 'index'])
        ->name('portfolios');

    Route::get('create', [PortfolioController::class, 'create'])
        ->name('portfolios.create');

    Route::post('', [PortfolioController::class, 'store'])
        ->name('portfolios.store');

    Route::get('{portfolio}', [PortfolioController::class, 'edit'])
        ->name('portfolios.edit');

    Route::patch('{portfolio}', [PortfolioController::class, 'update'])
        ->name('portfolios.update');

    Route::delete('{portfolio}', [PortfolioController::class, 'destroy'])
        ->name('portfolios.delete');
});
