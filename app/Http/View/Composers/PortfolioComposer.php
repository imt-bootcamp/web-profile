<?php

namespace App\Http\View\Composers;

use App\Models\Portfolio;
use Illuminate\View\View;

class PortfolioComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('webDevelopments', Portfolio::where('category_id', 1)->with('category')->latest()->get());
        $view->with('graphicDesigns', Portfolio::where('category_id', 2)->with('category')->latest()->get());
        $view->with('photographies', Portfolio::where('category_id', 3)->with('category')->latest()->get());
    }
}
