<?php

use App\Http\Controllers\CategoryController;

Route::group(['prefix' => 'categories'], function () {
    Route::get('', [CategoryController::class, 'index'])
        ->name('categories');

    Route::get('create', [CategoryController::class, 'create'])
        ->name('categories.create');

    Route::post('', [CategoryController::class, 'store'])
        ->name('categories.store');

    Route::get('{category}', [CategoryController::class, 'edit'])
        ->name('categories.edit');

    Route::patch('{category}', [CategoryController::class, 'update'])
        ->name('categories.update');

    Route::delete('{category}', [CategoryController::class, 'destroy'])
        ->name('categories.delete');
});
