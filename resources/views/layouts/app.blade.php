<!DOCTYPE html>

<html x-data="data()" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <title>@yield('title', env('APP_NAME'))</title>

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('windmill-panel/assets/css/tailwind.output.css') }}" />
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css"/> --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <style>
        [type='text'],
        [type='email'],
        [type='url'],
        [type='password'],
        [type='number'],
        [type='date'],
        [type='datetime-local'],
        [type='month'],
        [type='search'],
        [type='tel'],
        [type='time'],
        [type='week'],
        [multiple],
        textarea,
        select {
            border-color: #e2e8f0;
            border-width: 1px;
            border-radius: .25rem;
        }

        [type="file"] {
            background-color: #fff;
            border-color: #e2e8f0;
            border-width: 1px;
            border-radius: .25rem;
            padding: .5rem .75rem;
        }

        .progress {
            width: 24rem;
            height: .5rem;
            overflow: hidden;
            border-radius: .5rem;
            background: #f9fafb;
        }

        .progress__value {
            width: 0%;
            height: 100%;
            background: #7c3aed;
        }
    </style>

    @stack('styles')

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" defer></script> --}}
    <script src="{{ asset('windmill-panel/assets/js/init-alpine.js') }}"></script>
    {{-- <script src="{{ asset('windmill-panel/assets/js/charts-lines.js') }}" defer></script>
    <script src="{{ asset('windmill-panel/assets/js/charts-pie.js') }}" defer></script> --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>
    <div class="flex h-screen bg-gray-50 dark:bg-gray-900" :class="{ 'overflow-hidden': isSideMenuOpen }">
        @include('layouts.sidebar')
        <div class="flex flex-col flex-1 w-full">
            @include('layouts.header')
            <main class="h-full pb-16 overflow-y-auto">
                @yield('content')
            </main>
        </div>
    </div>

    @stack('scripts')

</body>
</html>
