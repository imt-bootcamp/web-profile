<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $profile = Profile::firstOrFail();

        return view('home.index', compact(
            'profile',
        ));
    }

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'subject' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/#contact')
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Gagal mengirim pesan, pastikan formulir terisi semua.');
        }

        Contact::create($request->all());

        return redirect('/#contact')->with('success', 'Pesan telah terkirim.');
    }
}
