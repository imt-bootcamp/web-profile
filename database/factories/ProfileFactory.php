<?php

namespace Database\Factories;

use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'avatar' => null,
            'name' => $this->faker->name,
            'birthdate' => '1990-04-20',
            'about' => $this->faker->sentence(25),
            'address' => $this->faker->sentence(6),
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'jobs' => "{$this->faker->jobTitle}, {$this->faker->jobTitle}, {$this->faker->jobTitle}",
            'cv' => null,
        ];
    }
}
