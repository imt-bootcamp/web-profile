<?php

use App\Http\Controllers\SettingController;

Route::group(['prefix' => 'settings'], function () {
    Route::get('', [SettingController::class, 'edit'])
        ->name('settings.edit');

    Route::patch('', [SettingController::class, 'update'])
        ->name('settings.update');
});
