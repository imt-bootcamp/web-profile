<div class="section" id="about">
    <div class="container">
        <div class="card" data-aos="fade-up" data-aos-offset="10">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card-body">
                        <div class="h4 mt-0 title">Tentang Saya</div>
                        <p>{{ $profile->about }}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card-body">
                        <div class="h4 mt-0 title">Biodata</div>
                        <div class="row">
                            <div class="col-sm-4"><strong class="text-uppercase">Umur</strong></div>
                            <div class="col-sm-8">{{ Carbon::parse($profile->birthdate)->age }}</div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-4"><strong class="text-uppercase">Email</strong></div>
                            <div class="col-sm-8">{{ $profile->email }}</div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-4"><strong class="text-uppercase">Nomor Telepon</strong></div>
                            <div class="col-sm-8">{{ $profile->phone }}</div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-4"><strong class="text-uppercase">Alamat</strong></div>
                            <div class="col-sm-8">{{ $profile->address }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
