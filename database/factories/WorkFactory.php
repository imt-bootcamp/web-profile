<?php

namespace Database\Factories;

use App\Models\Work;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Work::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dateStart = $this->faker->date();
        $dateEnd = strtotime('+1 year', strtotime($dateStart));
        $dateEnd = date('Y-m-d', $dateEnd);

        return [
            'company' => $this->faker->company,
            'position' => $this->faker->jobTitle,
            'description' => $this->faker->sentence(),
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
        ];
    }
}
