<?php

use App\Http\Controllers\DashboardController;

$groupOptoins = [
    'prefix' => 'admin',
    'middleware' => 'auth',
];

$routes = function () {
    Route::get('/', fn() => redirect()->route('dashboard'));

    Route::get('/dashboard', [DashboardController::class, 'index'])
        ->name('dashboard');

    include_once __DIR__ . '/profile.php';
    include_once __DIR__ . '/skills.php';
    include_once __DIR__ . '/socials.php';
    include_once __DIR__ . '/works.php';
    include_once __DIR__ . '/educations.php';
    include_once __DIR__ . '/categories.php';
    include_once __DIR__ . '/portfolios.php';
    include_once __DIR__ . '/contacts.php';
    include_once __DIR__ . '/settings.php';
};

Route::group($groupOptoins, $routes);
