<?php

namespace App\Http\Controllers;

use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SocialController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Socials - ' . env('APP_NAME');

        $whereArgs = ['name', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $socials = Social::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.socials.index', compact('title', 'socials'));
    }

    public function create()
    {
        $title = 'Create New Social - ' . env('APP_ENV');

        return view('admin.socials.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'url' => 'required|url',
            'svg' => 'required',
            'order' => 'required|integer',
            'visibility' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        Social::create($request->all());

        return redirect()->route('socials')->with('success', 'Data has been saved.');
    }

    public function edit(Social $social)
    {
        $data = $social;
        $title = "Edit Social {$social->name} - " . env('APP_NAME');

        return view('admin.socials.edit', compact('title', 'data'));
    }

    public function update(Social $social, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'url' => 'required|url',
            'svg' => 'required',
            'order' => 'required|integer',
            'visibility' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $social->update($request->all());

        return redirect()->route('socials')->with('success', 'Data has been updated.');
    }

    public function destroy(Social $social)
    {
        $social->delete();

        return redirect()->route('socials')->with('success', 'Data has been deleted.');
    }
}
