@if ($works->count())
    <div class="section" id="experience">
        <div class="container cc-experience">
            <div class="h4 text-center mb-4 title">Pengalaman Kerja</div>
            @foreach ($works as $work)
                <div class="card">
                    <div class="row">
                        <div class="col-md-3 bg-primary">
                            <div class="card-body cc-experience-header">
                                <p>{{ Carbon::parse($work->date_start)->format('M Y') }} - {{ $work->date_end ? Carbon::parse($work->date_end)->format('M Y') : 'Present' }}</p>
                                <div class="h5">{{ $work->company }}</div>
                            </div>
                        </div>
                        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                            <div class="card-body">
                                <div class="h5">{{ $work->position }}</div>
                                <p>{{ $work->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
