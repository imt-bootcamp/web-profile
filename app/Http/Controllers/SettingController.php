<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function edit()
    {
        $title = 'Settings - ' . env('APP_NAME');

        $data = User::firstOrFail();

        return view('admin.settings.edit', compact('title', 'data'));
    }

    public function update(Request $request)
    {
        $user = User::firstOrFail();
        $isRequestModified = false;

        $validatorRules = [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user),
            ],
            'password' => 'required|string|confirmed|min:8',
        ];

        if (!$request->password) {
            $request->merge([
                'password' => $user->password,
                'password_confirmation' =>  $user->password,
            ]);

            $isRequestModified = true;
        }

        $validator = Validator::make($request->all(), $validatorRules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        if (!$isRequestModified) {
            if (!Hash::check($request->password, $user->password)) {
                return back()
                    ->withInput()
                    ->withErrors(['password' => 'The password confirmation does not match.']);
            }

            $validatorRules['new_password'] = 'required|string|confirmed|min:8';

            $validator = Validator::make($request->all(), $validatorRules);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->with('error', 'Something went wrong.');
            }

            if (Hash::check($request->new_password, $user->password)) {
                return back()
                    ->withErrors($validator)
                    ->with('error', 'New password cannot be the same as your old password.');
            }

            $data['password'] = Hash::make($request->new_password);

            $user->update($data);

            return back()->with('success', 'Password has been changed.');
        }

        return back()->with('success', 'Data has been updated.');
    }
}
