<?php

namespace App\Http\Controllers;

use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Works - ' . env('APP_NAME');

        $whereArgs = ['company', 'LIKE', "%{$request->q}%"];
        $orderByArgs = [$request['order-by'] ?? 'created_at', $request->order ?? 'desc'];

        $works = Work::where(...$whereArgs)
            ->orderBy(...$orderByArgs)
            ->get();

        return view('admin.works.index', compact('title', 'works'));
    }

    public function create()
    {
        $title = 'Create New Work - ' . env('APP_ENV');

        return view('admin.works.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company' => 'required|max:255',
            'position' => 'required|max:255',
            'description' => 'required',
            'date_start' => 'required|date',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        Work::create($request->all());

        return redirect()->route('works')->with('success', 'Data has been saved.');
    }

    public function edit(Work $work)
    {
        $data = $work;
        $title = "Edit Work {$work->name} - " . env('APP_NAME');

        return view('admin.works.edit', compact('title', 'data'));
    }

    public function update(Work $work, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company' => 'required|max:255',
            'position' => 'required|max:255',
            'description' => 'required',
            'date_start' => 'required|date',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Something went wrong.');
        }

        $work->update($request->all());

        return redirect()->route('works')->with('success', 'Data has been updated.');
    }

    public function destroy(Work $work)
    {
        $work->delete();

        return redirect()->route('works')->with('success', 'Data has been deleted.');
    }
}
