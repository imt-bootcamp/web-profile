<footer class="footer">
    <div class="container text-center">
        @foreach ($socials as $social)
            @if ($social->visibility)
                <a class="footer-social" href="{{ $social->url }}">{!! $social->svg !!}</a>
            @endif
        @endforeach
    </div>
    <div class="h4 title text-center">{{ $profile->name }}</div>
    <div class="text-center text-muted">
        <p>&copy; Creative CV. All rights reserved. <br>Design - <a class="credit" href="https://templateflip.com" target="_blank">TemplateFlip</a></p>
    </div>
</footer>

<script src="{{ asset('resume-imt/js/core/jquery.3.2.1.min.js?ver=1.1.0') }}"></script>
<script src="{{ asset('resume-imt/js/core/popper.min.js?ver=1.1.0') }}"></script>
<script src="{{ asset('resume-imt/js/core/bootstrap.min.js?ver=1.1.0') }}"></script>
<script src="{{ asset('resume-imt/js/now-ui-kit.js?ver=1.1.0') }}"></script>
<script src="{{ asset('resume-imt/js/aos.js?ver=1.1.0') }}"></script>
<script src="{{ asset('resume-imt/scripts/main.js?ver=1.1.0') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

@if ($profile->hasManyJobs())
    <div id="typed-strings">
        @foreach ($profile->getJobs() as $job)
            <p>{{ $job }}</p>
        @endforeach
    </div>

    <script>
        var typed = new Typed('#typing-data', {
            stringsElement: '#typed-strings',
            typeSpeed: 36,
            startDelay: 250,
            backDelay: 400,
            backSpeed: 36,
            loop: true,
            smartBackspace: true,
            showCursor: false,
        });
    </script>
@endif

@stack('scripts')

</body>
</html>
