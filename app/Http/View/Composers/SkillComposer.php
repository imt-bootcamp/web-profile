<?php

namespace App\Http\View\Composers;

use App\Models\Skill;
use Illuminate\View\View;

class SkillComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('skillRows', Skill::latest()->get()->chunk(2));
    }
}
