<?php

namespace App\Http\View\Composers;

use App\Models\Work;
use Illuminate\View\View;

class WorkComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('works', Work::latest('date_end')->get());
    }
}
