<?php

namespace App\Http\View\Composers;

use App\Models\Profile;
use Illuminate\View\View;

class ProfileComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('profile', Profile::firstOrFail());
    }
}