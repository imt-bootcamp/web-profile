<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use App\Models\Social;
use App\Models\Contact;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $title = 'Dashboard - ' . env('APP_NAME');
        $skills = Skill::all();
        $socials = Social::all();
        $contacts = Contact::all();
        $portfolios = Portfolio::all();

        return view('admin.dashboard', compact('title', 'portfolios', 'skills', 'socials', 'contacts'));
    }
}
