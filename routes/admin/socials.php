<?php

use App\Http\Controllers\SocialController;

Route::group(['prefix' => 'socials'], function () {
    Route::get('', [SocialController::class, 'index'])
        ->name('socials');

    Route::get('create', [SocialController::class, 'create'])
        ->name('socials.create');

    Route::post('', [SocialController::class, 'store'])
        ->name('socials.store');

    Route::get('{social}', [SocialController::class, 'edit'])
        ->name('socials.edit');

    Route::patch('{social}', [SocialController::class, 'update'])
        ->name('socials.update');

    Route::delete('{social}', [SocialController::class, 'destroy'])
        ->name('socials.delete');
});
